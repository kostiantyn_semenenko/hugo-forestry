const slider = {
  slideItems() {
    function carouselInit() {
      $('.main-banner').slick({
        infinite: false,
        slidesToShow: 1,
      });
    }
    function init() {
      carouselInit();
    }
    return {
      init: init(),
    };
  },
};

export default slider;
