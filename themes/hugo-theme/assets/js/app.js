(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

var _hamburger = require('./modules/hamburger');

var _hamburger2 = _interopRequireDefault(_hamburger);

var _pageScroll = require('./modules/pageScroll');

var _pageScroll2 = _interopRequireDefault(_pageScroll);

var _carousel = require('./modules/carousel');

var _carousel2 = _interopRequireDefault(_carousel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(function ($) {
  // When DOM is ready
  $(function () {
    $('#currentYear').text('' + new Date().getFullYear());
    _pageScroll2.default.smoothScrolling();
    _hamburger2.default.handler();
    _carousel2.default.slideItems();
  });
})(jQuery); // You can write a call and import your functions in this file.
//
// This file will be compiled into app.js and will not be minified.
// Feel free with using ES6 here.

// import {NAME} from './modules/...';

},{"./modules/carousel":2,"./modules/hamburger":3,"./modules/pageScroll":4}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var slider = {
  slideItems: function slideItems() {
    function carouselInit() {
      $('.main-banner').slick({
        infinite: false,
        slidesToShow: 1
      });
    }
    function init() {
      carouselInit();
    }
    return {
      init: init()
    };
  }
};

exports.default = slider;

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var hamburger = {
  handler: function handler() {
    var $body = $('body');
    var $btn = $('.hamburger');
    var $menu = $('.menu');
    var $submenu = $('.submenu');
    var $submenuLink = $('.menu__link--submenu');
    var $submenuClose = $('.submenu__item--static');
    var OPENED_CLASS = 'opened';
    var OVERLAY_CLASS = 'overlay';
    function hamburgerToggle() {
      $btn.on('click', function switcher() {
        $(this).toggleClass(OPENED_CLASS);
        $menu.toggleClass(OPENED_CLASS);
        $body.toggleClass(OVERLAY_CLASS);
        $submenu.removeClass(OPENED_CLASS);
      });
    }
    function submenuToggle() {
      $submenuLink.on('click', function (event) {
        event.preventDefault();
        $submenu.toggleClass(OPENED_CLASS);
      });
      $submenuClose.on('click', function (event) {
        event.preventDefault();
        $submenu.toggleClass(OPENED_CLASS);
      });
    }
    function init() {
      if (!$btn.length) return;
      hamburgerToggle();
      if (!$submenu.length) return;
      submenuToggle();
    }
    return {
      init: init()
    };
  }
};

exports.default = hamburger;

},{}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var pageScroll = {
  smoothScrolling: function smoothScrolling() {
    var $window = $(window);
    var $page = $('html, body');
    var $body = $('body');
    var $menu = $('.menu');
    var $anchorLink = $('a[href^="#"]');
    var ANIMATION_SPEED = 400;
    var HEADER_HEIGHT = 0;
    var OVERFLOW_CLASS = 'overflow-hidden';
    var MOBILE_VIEW_ON = 1024;
    function scrolling() {
      $anchorLink.on('click', function scrollTo(event) {
        event.preventDefault();
        var $position = $($.attr(this, 'href')).offset().top;
        $page.animate({
          scrollTop: $position - HEADER_HEIGHT + 'px'
        }, ANIMATION_SPEED);
        if ($window.width() <= MOBILE_VIEW_ON) {
          $menu.fadeOut();
          $body.removeClass(OVERFLOW_CLASS);
        }
        return false;
      });
    }
    function init() {
      if (!$page.length) return;
      scrolling();
    }
    return {
      init: init()
    };
  }
};

exports.default = pageScroll;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJ0aGVtZXMvaHVnby10aGVtZS9zcmMvanMvYXBwLmpzIiwidGhlbWVzL2h1Z28tdGhlbWUvc3JjL2pzL21vZHVsZXMvY2Fyb3VzZWwuanMiLCJ0aGVtZXMvaHVnby10aGVtZS9zcmMvanMvbW9kdWxlcy9oYW1idXJnZXIuanMiLCJ0aGVtZXMvaHVnby10aGVtZS9zcmMvanMvbW9kdWxlcy9wYWdlU2Nyb2xsLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNNQTs7OztBQUNBOzs7O0FBQ0E7Ozs7OztBQUdBLENBQUMsVUFBQyxDQUFELEVBQU87QUFDTjtBQUNBLElBQUUsWUFBTTtBQUNOLE1BQUUsY0FBRixFQUFrQixJQUFsQixNQUEwQixJQUFJLElBQUosR0FBVyxXQUFYLEVBQTFCO0FBQ0EseUJBQVcsZUFBWDtBQUNBLHdCQUFVLE9BQVY7QUFDQSx1QkFBUyxVQUFUO0FBQ0QsR0FMRDtBQU1ELENBUkQsRUFRRyxNQVJILEUsQ0FYQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7QUNMQSxJQUFNLFNBQVM7QUFDYixZQURhLHdCQUNBO0FBQ1gsYUFBUyxZQUFULEdBQXdCO0FBQ3RCLFFBQUUsY0FBRixFQUFrQixLQUFsQixDQUF3QjtBQUN0QixrQkFBVSxLQURZO0FBRXRCLHNCQUFjO0FBRlEsT0FBeEI7QUFJRDtBQUNELGFBQVMsSUFBVCxHQUFnQjtBQUNkO0FBQ0Q7QUFDRCxXQUFPO0FBQ0wsWUFBTTtBQURELEtBQVA7QUFHRDtBQWRZLENBQWY7O2tCQWlCZSxNOzs7Ozs7OztBQ2pCZixJQUFNLFlBQVk7QUFDaEIsU0FEZ0IscUJBQ047QUFDUixRQUFNLFFBQVEsRUFBRSxNQUFGLENBQWQ7QUFDQSxRQUFNLE9BQU8sRUFBRSxZQUFGLENBQWI7QUFDQSxRQUFNLFFBQVEsRUFBRSxPQUFGLENBQWQ7QUFDQSxRQUFNLFdBQVcsRUFBRSxVQUFGLENBQWpCO0FBQ0EsUUFBTSxlQUFlLEVBQUUsc0JBQUYsQ0FBckI7QUFDQSxRQUFNLGdCQUFnQixFQUFFLHdCQUFGLENBQXRCO0FBQ0EsUUFBTSxlQUFlLFFBQXJCO0FBQ0EsUUFBTSxnQkFBZ0IsU0FBdEI7QUFDQSxhQUFTLGVBQVQsR0FBMkI7QUFDekIsV0FBSyxFQUFMLENBQVEsT0FBUixFQUFpQixTQUFTLFFBQVQsR0FBb0I7QUFDbkMsVUFBRSxJQUFGLEVBQVEsV0FBUixDQUFvQixZQUFwQjtBQUNBLGNBQU0sV0FBTixDQUFrQixZQUFsQjtBQUNBLGNBQU0sV0FBTixDQUFrQixhQUFsQjtBQUNBLGlCQUFTLFdBQVQsQ0FBcUIsWUFBckI7QUFDRCxPQUxEO0FBTUQ7QUFDRCxhQUFTLGFBQVQsR0FBeUI7QUFDdkIsbUJBQWEsRUFBYixDQUFnQixPQUFoQixFQUF5QixVQUFDLEtBQUQsRUFBVztBQUNsQyxjQUFNLGNBQU47QUFDQSxpQkFBUyxXQUFULENBQXFCLFlBQXJCO0FBQ0QsT0FIRDtBQUlBLG9CQUFjLEVBQWQsQ0FBaUIsT0FBakIsRUFBMEIsVUFBQyxLQUFELEVBQVc7QUFDbkMsY0FBTSxjQUFOO0FBQ0EsaUJBQVMsV0FBVCxDQUFxQixZQUFyQjtBQUNELE9BSEQ7QUFJRDtBQUNELGFBQVMsSUFBVCxHQUFnQjtBQUNkLFVBQUksQ0FBQyxLQUFLLE1BQVYsRUFBa0I7QUFDbEI7QUFDQSxVQUFJLENBQUMsU0FBUyxNQUFkLEVBQXNCO0FBQ3RCO0FBQ0Q7QUFDRCxXQUFPO0FBQ0wsWUFBTTtBQURELEtBQVA7QUFHRDtBQXJDZSxDQUFsQjs7a0JBd0NlLFM7Ozs7Ozs7O0FDeENmLElBQU0sYUFBYTtBQUNqQixpQkFEaUIsNkJBQ0M7QUFDaEIsUUFBTSxVQUFVLEVBQUUsTUFBRixDQUFoQjtBQUNBLFFBQU0sUUFBUSxFQUFFLFlBQUYsQ0FBZDtBQUNBLFFBQU0sUUFBUSxFQUFFLE1BQUYsQ0FBZDtBQUNBLFFBQU0sUUFBUSxFQUFFLE9BQUYsQ0FBZDtBQUNBLFFBQU0sY0FBYyxFQUFFLGNBQUYsQ0FBcEI7QUFDQSxRQUFNLGtCQUFrQixHQUF4QjtBQUNBLFFBQU0sZ0JBQWdCLENBQXRCO0FBQ0EsUUFBTSxpQkFBaUIsaUJBQXZCO0FBQ0EsUUFBTSxpQkFBaUIsSUFBdkI7QUFDQSxhQUFTLFNBQVQsR0FBcUI7QUFDbkIsa0JBQVksRUFBWixDQUFlLE9BQWYsRUFBd0IsU0FBUyxRQUFULENBQWtCLEtBQWxCLEVBQXlCO0FBQy9DLGNBQU0sY0FBTjtBQUNBLFlBQU0sWUFBWSxFQUFFLEVBQUUsSUFBRixDQUFPLElBQVAsRUFBYSxNQUFiLENBQUYsRUFBd0IsTUFBeEIsR0FBaUMsR0FBbkQ7QUFDQSxjQUFNLE9BQU4sQ0FBYztBQUNaLHFCQUFjLFlBQVksYUFBMUI7QUFEWSxTQUFkLEVBRUcsZUFGSDtBQUdBLFlBQUksUUFBUSxLQUFSLE1BQW1CLGNBQXZCLEVBQXVDO0FBQ3JDLGdCQUFNLE9BQU47QUFDQSxnQkFBTSxXQUFOLENBQWtCLGNBQWxCO0FBQ0Q7QUFDRCxlQUFPLEtBQVA7QUFDRCxPQVhEO0FBWUQ7QUFDRCxhQUFTLElBQVQsR0FBZ0I7QUFDZCxVQUFJLENBQUMsTUFBTSxNQUFYLEVBQW1CO0FBQ25CO0FBQ0Q7QUFDRCxXQUFPO0FBQ0wsWUFBTTtBQURELEtBQVA7QUFHRDtBQWhDZ0IsQ0FBbkI7O2tCQW1DZSxVIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKXtmdW5jdGlvbiByKGUsbix0KXtmdW5jdGlvbiBvKGksZil7aWYoIW5baV0pe2lmKCFlW2ldKXt2YXIgYz1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlO2lmKCFmJiZjKXJldHVybiBjKGksITApO2lmKHUpcmV0dXJuIHUoaSwhMCk7dmFyIGE9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitpK1wiJ1wiKTt0aHJvdyBhLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsYX12YXIgcD1uW2ldPXtleHBvcnRzOnt9fTtlW2ldWzBdLmNhbGwocC5leHBvcnRzLGZ1bmN0aW9uKHIpe3ZhciBuPWVbaV1bMV1bcl07cmV0dXJuIG8obnx8cil9LHAscC5leHBvcnRzLHIsZSxuLHQpfXJldHVybiBuW2ldLmV4cG9ydHN9Zm9yKHZhciB1PVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmUsaT0wO2k8dC5sZW5ndGg7aSsrKW8odFtpXSk7cmV0dXJuIG99cmV0dXJuIHJ9KSgpIiwiLy8gWW91IGNhbiB3cml0ZSBhIGNhbGwgYW5kIGltcG9ydCB5b3VyIGZ1bmN0aW9ucyBpbiB0aGlzIGZpbGUuXG4vL1xuLy8gVGhpcyBmaWxlIHdpbGwgYmUgY29tcGlsZWQgaW50byBhcHAuanMgYW5kIHdpbGwgbm90IGJlIG1pbmlmaWVkLlxuLy8gRmVlbCBmcmVlIHdpdGggdXNpbmcgRVM2IGhlcmUuXG5cbi8vIGltcG9ydCB7TkFNRX0gZnJvbSAnLi9tb2R1bGVzLy4uLic7XG5pbXBvcnQgaGFtYnVyZ2VyIGZyb20gJy4vbW9kdWxlcy9oYW1idXJnZXInO1xuaW1wb3J0IHBhZ2VTY3JvbGwgZnJvbSAnLi9tb2R1bGVzL3BhZ2VTY3JvbGwnO1xuaW1wb3J0IGNhcm91c2VsIGZyb20gJy4vbW9kdWxlcy9jYXJvdXNlbCc7XG5cblxuKCgkKSA9PiB7XG4gIC8vIFdoZW4gRE9NIGlzIHJlYWR5XG4gICQoKCkgPT4ge1xuICAgICQoJyNjdXJyZW50WWVhcicpLnRleHQoYCR7bmV3IERhdGUoKS5nZXRGdWxsWWVhcigpfWApO1xuICAgIHBhZ2VTY3JvbGwuc21vb3RoU2Nyb2xsaW5nKCk7XG4gICAgaGFtYnVyZ2VyLmhhbmRsZXIoKTtcbiAgICBjYXJvdXNlbC5zbGlkZUl0ZW1zKCk7XG4gIH0pO1xufSkoalF1ZXJ5KTtcbiIsImNvbnN0IHNsaWRlciA9IHtcbiAgc2xpZGVJdGVtcygpIHtcbiAgICBmdW5jdGlvbiBjYXJvdXNlbEluaXQoKSB7XG4gICAgICAkKCcubWFpbi1iYW5uZXInKS5zbGljayh7XG4gICAgICAgIGluZmluaXRlOiBmYWxzZSxcbiAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxuICAgICAgfSk7XG4gICAgfVxuICAgIGZ1bmN0aW9uIGluaXQoKSB7XG4gICAgICBjYXJvdXNlbEluaXQoKTtcbiAgICB9XG4gICAgcmV0dXJuIHtcbiAgICAgIGluaXQ6IGluaXQoKSxcbiAgICB9O1xuICB9LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgc2xpZGVyO1xuIiwiY29uc3QgaGFtYnVyZ2VyID0ge1xuICBoYW5kbGVyKCkge1xuICAgIGNvbnN0ICRib2R5ID0gJCgnYm9keScpO1xuICAgIGNvbnN0ICRidG4gPSAkKCcuaGFtYnVyZ2VyJyk7XG4gICAgY29uc3QgJG1lbnUgPSAkKCcubWVudScpO1xuICAgIGNvbnN0ICRzdWJtZW51ID0gJCgnLnN1Ym1lbnUnKTtcbiAgICBjb25zdCAkc3VibWVudUxpbmsgPSAkKCcubWVudV9fbGluay0tc3VibWVudScpO1xuICAgIGNvbnN0ICRzdWJtZW51Q2xvc2UgPSAkKCcuc3VibWVudV9faXRlbS0tc3RhdGljJyk7XG4gICAgY29uc3QgT1BFTkVEX0NMQVNTID0gJ29wZW5lZCc7XG4gICAgY29uc3QgT1ZFUkxBWV9DTEFTUyA9ICdvdmVybGF5JztcbiAgICBmdW5jdGlvbiBoYW1idXJnZXJUb2dnbGUoKSB7XG4gICAgICAkYnRuLm9uKCdjbGljaycsIGZ1bmN0aW9uIHN3aXRjaGVyKCkge1xuICAgICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKE9QRU5FRF9DTEFTUyk7XG4gICAgICAgICRtZW51LnRvZ2dsZUNsYXNzKE9QRU5FRF9DTEFTUyk7XG4gICAgICAgICRib2R5LnRvZ2dsZUNsYXNzKE9WRVJMQVlfQ0xBU1MpO1xuICAgICAgICAkc3VibWVudS5yZW1vdmVDbGFzcyhPUEVORURfQ0xBU1MpO1xuICAgICAgfSk7XG4gICAgfVxuICAgIGZ1bmN0aW9uIHN1Ym1lbnVUb2dnbGUoKSB7XG4gICAgICAkc3VibWVudUxpbmsub24oJ2NsaWNrJywgKGV2ZW50KSA9PiB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICRzdWJtZW51LnRvZ2dsZUNsYXNzKE9QRU5FRF9DTEFTUyk7XG4gICAgICB9KTtcbiAgICAgICRzdWJtZW51Q2xvc2Uub24oJ2NsaWNrJywgKGV2ZW50KSA9PiB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICRzdWJtZW51LnRvZ2dsZUNsYXNzKE9QRU5FRF9DTEFTUyk7XG4gICAgICB9KTtcbiAgICB9XG4gICAgZnVuY3Rpb24gaW5pdCgpIHtcbiAgICAgIGlmICghJGJ0bi5sZW5ndGgpIHJldHVybjtcbiAgICAgIGhhbWJ1cmdlclRvZ2dsZSgpO1xuICAgICAgaWYgKCEkc3VibWVudS5sZW5ndGgpIHJldHVybjtcbiAgICAgIHN1Ym1lbnVUb2dnbGUoKTtcbiAgICB9XG4gICAgcmV0dXJuIHtcbiAgICAgIGluaXQ6IGluaXQoKSxcbiAgICB9O1xuICB9LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgaGFtYnVyZ2VyO1xuIiwiY29uc3QgcGFnZVNjcm9sbCA9IHtcbiAgc21vb3RoU2Nyb2xsaW5nKCkge1xuICAgIGNvbnN0ICR3aW5kb3cgPSAkKHdpbmRvdyk7XG4gICAgY29uc3QgJHBhZ2UgPSAkKCdodG1sLCBib2R5Jyk7XG4gICAgY29uc3QgJGJvZHkgPSAkKCdib2R5Jyk7XG4gICAgY29uc3QgJG1lbnUgPSAkKCcubWVudScpO1xuICAgIGNvbnN0ICRhbmNob3JMaW5rID0gJCgnYVtocmVmXj1cIiNcIl0nKTtcbiAgICBjb25zdCBBTklNQVRJT05fU1BFRUQgPSA0MDA7XG4gICAgY29uc3QgSEVBREVSX0hFSUdIVCA9IDA7XG4gICAgY29uc3QgT1ZFUkZMT1dfQ0xBU1MgPSAnb3ZlcmZsb3ctaGlkZGVuJztcbiAgICBjb25zdCBNT0JJTEVfVklFV19PTiA9IDEwMjQ7XG4gICAgZnVuY3Rpb24gc2Nyb2xsaW5nKCkge1xuICAgICAgJGFuY2hvckxpbmsub24oJ2NsaWNrJywgZnVuY3Rpb24gc2Nyb2xsVG8oZXZlbnQpIHtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgY29uc3QgJHBvc2l0aW9uID0gJCgkLmF0dHIodGhpcywgJ2hyZWYnKSkub2Zmc2V0KCkudG9wO1xuICAgICAgICAkcGFnZS5hbmltYXRlKHtcbiAgICAgICAgICBzY3JvbGxUb3A6IGAkeyRwb3NpdGlvbiAtIEhFQURFUl9IRUlHSFR9cHhgLFxuICAgICAgICB9LCBBTklNQVRJT05fU1BFRUQpO1xuICAgICAgICBpZiAoJHdpbmRvdy53aWR0aCgpIDw9IE1PQklMRV9WSUVXX09OKSB7XG4gICAgICAgICAgJG1lbnUuZmFkZU91dCgpO1xuICAgICAgICAgICRib2R5LnJlbW92ZUNsYXNzKE9WRVJGTE9XX0NMQVNTKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9KTtcbiAgICB9XG4gICAgZnVuY3Rpb24gaW5pdCgpIHtcbiAgICAgIGlmICghJHBhZ2UubGVuZ3RoKSByZXR1cm47XG4gICAgICBzY3JvbGxpbmcoKTtcbiAgICB9XG4gICAgcmV0dXJuIHtcbiAgICAgIGluaXQ6IGluaXQoKSxcbiAgICB9O1xuICB9LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgcGFnZVNjcm9sbDtcbiJdfQ==
